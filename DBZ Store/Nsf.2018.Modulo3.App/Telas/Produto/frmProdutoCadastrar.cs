﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.D_B_D;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmProdutoCadastrar : UserControl
    {
        public frmProdutoCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            DBZ_DTO dto = new DBZ_DTO();
            dto.Nome = txtProduto.Text;
            dto.Preco = Convert.ToDouble(txtPreco.Text);
            DBZ_Business business = new DBZ_Business();
            business.Salvar(dto);
            MessageBox.Show("Produto Cadrastado com sucesso", "Cadastramento de Novo Produto", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Hide();
        }
    }
}
