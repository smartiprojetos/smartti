﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf._2018.Modulo3.App.D_B_D;

namespace Nsf._2018.Modulo3.App.Telas
{
    public partial class frmPedidoCadastrar : UserControl
    {
        BindingList<DBZ_DTO> produtosCarrinho = new BindingList<DBZ_DTO>();
        public frmPedidoCadastrar()
        {
            InitializeComponent();
            /*Foram criadas duas funções para que se tornasse possivel */
            CarregarCombos(); /* Carrega as variaveis do dto do produto */
            ConfigurarGrid();/*Configurações da grid da tela*/
            
        }

        void CarregarCombos()
        {
            DBZ_Business business = new DBZ_Business();
            List<DBZ_DTO> lista = business.Listar();

            cboProduto.ValueMember = nameof(DBZ_DTO.ID);
            cboProduto.DisplayMember = nameof(DBZ_DTO.Nome);
            cboProduto.DataSource = lista;

        }
        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void btnEmitir_Click(object sender, EventArgs e)
        {
            DBZ_DTO dto = new DBZ_DTO();
            dto.Nome = txtCliente.Text;
            dto.Cpf = txtCpf.Text;
            dto.Data = DateTime.Now;

            DBZ_Business business = new DBZ_Business();
            business.Salvar(dto, produtosCarrinho.ToList());
            MessageBox.Show("Pedido Salvo com Sucesso", "Pedido", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                        
            this.Hide();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DBZ_DTO dto = cboProduto.SelectedItem as DBZ_DTO;

            for (int i = 0; i < int.Parse(txtQuantidade.Text); i++)
            {
                produtosCarrinho.Add(dto);
            }
        }
    }
}
