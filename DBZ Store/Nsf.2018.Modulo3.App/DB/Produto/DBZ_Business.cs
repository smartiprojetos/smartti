﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.D_B_D
{
    class DBZ_Business
    {
        public int Salvar(DBZ_DTO dto)
        {
            DBZ_Database db = new DBZ_Database();
            return db.Salvar(dto);
        }

        public List<DBZ_DTO> Listar()
        {
            DBZ_DTO db = new DBZ_DTO();
            return db.Listar();
        }
    }
}
