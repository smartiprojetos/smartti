﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.D_B_D
{
    class DBZ_Database
    {
        public int Salvar(DBZ_DTO dto)
        {
            string script =
                @"insert into tb_produto(nm_produto,vl_preco )
                values(@nm_produto,@vl_preco) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Nome));
            parms.Add(new MySqlParameter("nm_produto", dto.Preco));
            Database db = new Database();




            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public List<DBZ_DTO> Listar()
        {
            string script =
                @"Select * from tb_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DBZ_DTO> lista = new List<DBZ_DTO>();

            while (reader.Read())
            {
                DBZ_DTO dto = new DBZ_DTO();
                dto.ID = reader.GetInt32("id_produto");
                dto.Nome = reader.GetString("nm_produto");
                dto.Preco = reader.GetDouble("vl_preco");
                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }
    }
}
