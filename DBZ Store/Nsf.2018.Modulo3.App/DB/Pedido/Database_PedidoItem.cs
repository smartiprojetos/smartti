﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class Database_PedidoItem
    {
        /*A database do Pedido Item irá fazer a relação entre o item que foi pedido e quem pediu , no pedido 
         temos as informações do cliente e no produto as informações dos produtos que podem ser pedidos */
        public int Salvar(DTO_PedidoItem dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_produto,id_pedido) values (@id_produto,@id_pedido) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto", dto.IDProduto));
            parms.Add(new MySqlParameter("id_pedido", dto.IDPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
    }
}
