﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class Database_Pedido
    {
        /*Na database do pedido será adicionado os valores referentes a quem realizou a compra como o nome, cpf e data
         cada valor será retirado da tela passará pelo dto por meio do business e depois para a database
         Na database (aqui) o script irá enviar para o MySQL (se estiver funcionando tudo) por meio da Database e da conection
         (não é a mesma database não)
         */
        public int Salvar(DTO_Pedido dto)
        {
            string script = @"INSERT INTO tb_pedido(nm_cliente, ds_cpf, dt_venda) values (@nm_cliente, @ds_cpf, @dt_venda)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);



        }

    }
}
