﻿using Nsf._2018.Modulo3.App.D_B_D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class Business_Pedido
    {
        /*Aqui será realizado a escrita do codigo de salvamento dos produtos , ele 
         irá ligar o dto e o database de modo simples , pois pegará os dados que a tela adicionou no dto,
         passará os dados aqui e enviará para o database que executará o script que enviará para o MySql*/

        public int Salvar(DTO_Pedido pedido , List<DTO_Pedido> produtos)
        {
            Database_Pedido pedidodatabase = new Database_Pedido();
            int idPedido = pedidodatabase.Salvar(pedido);

            /* foreach */
            Business_Pedido itembusiness = new Business_Pedido();
            
            foreach ( DBZ_DTO item in produto)
            {
                DTO_PedidoItem itemdto = new DTO_PedidoItem();
                itemdto.IDPedido = idPedido;
                itemdto.IDProduto = item.ID ;
                /*salva tudo que foi salvo no dto do pedidoitem*/
                itembusiness.Salvar(itemdto);
              
            }
            return idPedido;

        }
    }
}
