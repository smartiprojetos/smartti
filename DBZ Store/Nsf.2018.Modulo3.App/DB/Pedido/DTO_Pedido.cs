﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.App.DB.Pedido
{
    class DTO_Pedido
    {
        /* No pedido tem que ter o id, o cliente, cpf e a data 
            Esse pedido é o que irá colocar lá na telinha de Pedido Cadastrar 
         Os campos de lá prescisam de cliente e data , logo o cpf é só registro do banco e não o que irá aparecer
         
         */
        public int ID { get; set; }
        public string Cliente { get; set; }
        public string Cpf { get; set; }
        public DateTime Data { get; set; }
        /*
         Id será auto_increment , cliente o nome do cliente cadastrado
         Cpf o cpf do cliente cadastrado e data a data do horario em que o botõa foi clikado
         */
    }
}
