﻿create database DBZStoreDB;

use DBZStoreDB;


create table tb_produto (
	id_produto int primary key auto_increment,
    nm_produto varchar(100),
    vl_preco decimal(15,2)
);

select * from tb_produto;

create table tb_pedido (
	id_pedido int primary key auto_increment,
	nm_cliente varchar(50),
	ds_cpf int(12),
	dt_venda date,
	)

create table tb_pedido_item(
	id_pedido_item
	CONSTRAINT fk_PItemProduto FOREIGN KEY (id_produto) REFERENCES tb_produto (id_produto)
	CONSTRAINT fk_PItemPedido FOREIGN KEY (id_pedido) REFERENCES tb_pedido (id_pedido)
	)
